// scripts/main.js
console.log('page load, entered main.js');
var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo() {
	console.log('entered getFormInfo!');

	// get all story details
	var title_text = document.getElementById('title-text').value;
	var author_text = document.getElementById('author-text').value;
	var story_text = document.getElementById('text-story').value;
	console.log('title: ' + title_text + ' author: ' + author_text + ' story: ' + story_text);

	var genres_string = "";
	if(document.getElementById("checkbox-horror-value").checked) {
		genres_string += "Horror, ";
	}

	if(document.getElementById("checkbox-comedy-value").checked) {
		genres_string += "Comedy, ";
	}

	if(document.getElementById("checkbox-fantasy-value").checked) {
		genres_string += "Fantasy, ";
	}

	console.log('genres: ' + genres_string);

	// create a story dictionary
	var story_dict = {};
	story_dict['title'] = title_text;
	story_dict['author'] = author_text;
	story_dict['story'] = story_text;
	story_dict['genres'] = genres_string;

	console.log(story_dict);

	// call display info
	displayInfo(story_dict);
}

function displayInfo(story_dict) {
	console.log('entered displayInfo');
	var story_top = document.getElementById('story-top-line');
	story_top.innerHTML = story_dict['title'] + ' in ' + story_dict['author'];

	var story_body = document.getElementById('story-body');
	story_body.innerHTML = story_dict['story'];
}


/*document.getElementById("myButton").onmouseup = changeLabelText;

function changeLabelText() {
	console.log('entered changeLabelText');
	var label1 = document.getElementById("myLabel");
	label1.innerHTML = "Paradigms class";
}
*/