# NickandMary_Paradigms_Final_Project

- mmccann6@nd.edu
- nmarsha3@nd.edu

Nick Marshall and Mary McCann's final project for Programming Paradigms. Building a REST Server for crime statistics in Boston.

## Data Source
- [Boston Crime Statistics](https://data.boston.gov/dataset/crime-incident-reports-august-2015-to-date-source-new-system/resource/12cb3883-56f5-47de-afa5-3b1cf61b257b)

## JSON Specification
- [Our Google Doc](https://docs.google.com/document/d/1JGbqtXMWf2DQzp-ttremDoxDcY_beqf3Qd0LHXnzio0/edit?usp=sharing)

## OO API
Our current API comes with the standard REST features: GET, PUT, POST, and DELETE (with our crime controller). 
- For GET: you have two options. You can get the specific crimes incident_number (GET_KEY) or you can get the entire database (GET_INDEX)
- To add/manipulate data:
    - PUT: takes in an incident number, and you can manipulate that incident number's type of crime (PUT_KEY)
    - POST: you can post a new crime entry, adding a crime to the database (like a reporting system) (POST_INDEX)
- There are two DELETE options: delete a single crime by its incident number (DELETE_KEY) or you can delete the entire data base (DELETE_INDEX)
We also have a reset controller that has two handlers
- PUT_KEY: resets a given incident number to its original crime type
- PUT_INDEX: resets entire data base back to its original state

## How to Run the Tests
- The server runs on student13, port 51069
- You must be using the correct python3 path (provided in class)
- We have three tests: test_crimes_key.py, test_crimes_index.py, and test_crimes_reset.py. These are located in the ooapi directory
    - test_crimes_key.py: tests the key handlers
    - test_crimes_index.py: tests the handlers that deal with the index
    - test_crimes_reset.py: tests all the reset handlers
- Use python3 to run the tests

## User Interaction
- The server runs on student13, port 51069
- The BostonCrimeWebsite.html file can be accessed through the pages tab on this GitLab repo (http://nmarsha3.gitlab.io/nickandmary_paradigms_final_project/jsfrontend/BostonCrimeWebsite.html). Make sure that the url is http and not https.
- When you load up the website to the main page we have a short paragraph about our objectives as well as some famous Boston crime stories that you can click on to learn more.
- When you click the crimes tab you can select a type of crime from a dropdown and the website will output the number of that type of crime as well as a description of each incident.
- When you click on the "report a crime" tab you have the oppurtunity to write in all the information associated with a new crime to add to the database. It outputs if the request was successful or not to you.
- When you click on the incidents tab you can type a specific incident number and the website outputs all of the data associated with that incident. Also, a google map drops a marker at the exact latitude and longitude that the crime occurred (Note that the page says that "the page can't load Google Maps correctly" but this is not true, it does work every time. This happens because we did not want to purchase the full version).
- When you click on the chart tab you see a chart of the notable types of crimes. When you scroll over each bar you can see the exact number of each type.

## Videos and Slides
- [Project Demo](https://drive.google.com/file/d/1LcgeBeX_98CtYVDIg4GtNImVdi0rb17H/view)
- [Code Walkthrough](https://drive.google.com/file/d/1OMs2U-NrPgfMbbl8LE4b3rhx-C7qI2rx/view)
- [Presentation Slides](https://docs.google.com/presentation/d/1Wbw7bvSSe3j8EL60Z-KVYmsCOB-UFNIXffp5QoLx3-g/edit#slide=id.p)

## Complexity and Scope
This project incorporates many of the topics learned from our course. First, we utilized object-oriented programming when creating the controllers and crime library. This allowed for better organization of our code and made it easy to add functionality later as the project grew. We also have a client-server relationship with our HTML/JavaScript front end acting as the client, making requests to our server, which in turn uses our controllers. Specifically for the front end we went above and beyond learning outside of class how to add features such as charts and maps. Also, our JavaScript files were more advanced as we our website had more complicated functionality than what we had done in the "dailys".