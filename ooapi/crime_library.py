class _crime_database:

       def __init__(self):

        # INCIDENT_NUMBER,OFFENSE_CODE,OFFENSE_CODE_GROUP,OFFENSE_DESCRIPTION,DISTRICT,REPORTING_AREA,SHOOTING,OCCURRED_ON_DATE,YEAR,MONTH,
        # DAY_OF_WEEK,HOUR,UCR_PART,STREET,Lat,Long,Location
        
        #self.incident_number = dict()
        self.offense_code = dict()
        self.offense_code_group = dict()
        self.offense_description = dict()
        self.district = dict()
        self.reporting_area = dict()
        self.shooting = dict()
        self.occurred_on_date = dict()
        self.year = dict()
        self.month = dict()
        self.day_of_week = dict()
        self.hour = dict()
        self.ucr_part = dict()
        self.street = dict()
        self.latitude = dict()
        self.longitude = dict()
        self.location = dict()

       def load_crimes(self, boston_crime_file):
        # open file
        f = open(boston_crime_file)

        # go thru data and organize into a data structure
        for line in f:
                line = line.rstrip()
                components = line.split(",")

                # grab key
                incident_number         = components[0]

                # grab values
                offense_code            = components[1]
                offense_code_group      = components[2]
                offense_description     = components[3]
                district                = components[4]
                reporting_area          = components[5]
                shooting                = components[6]
                occurred_on_date        = components[7]
                year                    = components[8]
                month                   = components[9]
                day_of_week             = components[10]
                hour                    = components[11]
                ucr_part                = components[12]
                street                  = components[13]
                latitude                = components[14]
                longitude               = components[15]
                location                = components[16]
                

                #print("The shooting is", shooting, "and the report area is", reporting_area, "and incident number is", incident_number, "!")
                
                self.offense_code[incident_number] = offense_code
                self.offense_code_group[incident_number] = offense_code_group
                self.offense_description[incident_number] = offense_description
                self.district[incident_number] = district
                self.reporting_area[incident_number] = reporting_area
                self.shooting[incident_number] = shooting
                self.occurred_on_date[incident_number] = occurred_on_date
                self.year[incident_number] = year
                self.month[incident_number] = month
                self.day_of_week[incident_number] = day_of_week
                self.hour[incident_number] = hour
                self.ucr_part[incident_number] = ucr_part
                self.street[incident_number] = street
                self.latitude[incident_number] = latitude
                self.longitude[incident_number] = longitude
                self.location[incident_number] = location
                
        f.close()

       def get_crimes(self):
        return self.offense_code.keys() # returns a list of incident numbers

       def get_crime_type(self, incident_number):
        try:
                #print("in get_crime_type")
                #print(incident_number)
                incident_number = str(incident_number)
                #print(self.offense_description)
                #print(self.offense_description[incident_number])
                c_offense_description = self.offense_description[incident_number]
                #print(c_offense_description)
        except Exception as ex:
                c_offense_description = None
        return c_offense_description

       def get_shooting(self, incident_number):
          try:
             incident_number = str(incident_number)
             c_shooting = self.shooting[incident_number]
          except Exception as ex:
             c_shooting = None
          return c_shooting

       def get_street(self, incident_number):
          try:
             incident_number = str(incident_number)
             c_street = self.street[incident_number]
          except Exception as ex:
             c_street = None
          return c_street
       
       def get_occurred_on_date(self, incident_number):
          try:
             incident_number = str(incident_number)
             c_occurred_on_date = self.occurred_on_date[incident_number]
          except Exception as ex:
             c_occurred_on_date = None
          return c_occurred_on_date
       
       def get_coordinates(self, incident_number):
          try:
             incident_number = str(incident_number)
             c_latitude = self.latitude[incident_number]
             c_longitude = self.longitude[incident_number]
          except Exception as ex:
             c_latitude = None
             c_longitude = None
          return c_latitude, c_longitude

       def set_crime(self, incident_number, crime):
        
        #print("In set crime")
        #print(incident_number)
        #print(crime)
        self.offense_code[incident_number] = crime[0]
        self.offense_code_group[incident_number] = crime[1]
        self.offense_description[incident_number] = crime[2]
        self.district[incident_number] = crime[3]
        self.reporting_area[incident_number] = crime[4]
        self.shooting[incident_number] = crime[5]
        self.occurred_on_date[incident_number] = crime[6]
        self.year[incident_number] = crime[7]
        self.month[incident_number] = crime[8]
        self.day_of_week[incident_number] = crime[9]
        self.hour[incident_number] = crime[10]
        self.ucr_part[incident_number] = crime[11]
        self.street[incident_number] = crime[12]
        self.latitude[incident_number] = crime[13]
        self.longitude[incident_number] = crime[14]
        self.location[incident_number] = crime[15]

       def delete_crime(self, incident_number):
        del(self.offense_code[incident_number])
        del(self.offense_code_group[incident_number])
        del(self.offense_description[incident_number])
        del(self.district[incident_number])
        del(self.reporting_area[incident_number])
        del(self.shooting[incident_number])
        del(self.occurred_on_date[incident_number])
        del(self.year[incident_number])
        del(self.month[incident_number])
        del(self.day_of_week[incident_number])
        del(self.hour[incident_number])
        del(self.ucr_part[incident_number])
        del(self.street[incident_number])
        del(self.latitude[incident_number])
        del(self.longitude[incident_number])
        del(self.location[incident_number])


if __name__ == "__main__":
       mdb = _crime_database()

       #### MOVIES ####
       mdb.load_crimes('bostonCrime.csv')

       crime = mdb.get_crime_type('I182029536')
       print(crime)

       mdb.set_crime("New Offense Code", "1", "Scary Crime", "some district", "some reporting area", "shooting", "some date", "this is a year", "this is a month", "this is a day of the week", "this is a hour", "this is a ucr part", "this is a street", "this is a latitude", "this is a longitude", "this is a location")
       
       another_crime = mbd.get_crime_type("New Offense Code")
       print(another_crime)

