import cherrypy
import re, json
from ooapi.crime_library import _crime_database

class ResetController(object):
    def __init__(self, mdb=None):
        if mdb is None:
            self.mdb = _crime_database()
        else:
            self.mdb = mdb

    def PUT_INDEX(self):
        '''when PUT request comes in to /reset/ endpoint, then the crime database is reloaded'''
        output = {'result':'success'}

        self.mdb.__init__()
        
        # TODO: fix the boston crimes locatoin
        self.mdb.load_crimes('data/bostonCrime.csv')

        return json.dumps(output)

    def PUT_KEY(self, incident):
        '''when PUT request comes in for /reset/movie_id endpoint, then that movie is reloaded and updated in mdb'''
        output = {'result':'success'}

        try:
            mdbtmp = _crime_database()
            mdbtmp.load_crimes('data/bostonCrime.csv')
            
            incident = str(incident)
            print("In RESET CONTROLLER")
            print(incident)
            crime_type = mdbtmp.get_crime_type(incident)
            print("RESET: After get crime type")
            print(crime_type)

            crime = ["", "", crime_type, "", "", "", "", "", "", "", "", "", "", "", "", ""]

            self.mdb.set_crime(incident, crime)

        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
