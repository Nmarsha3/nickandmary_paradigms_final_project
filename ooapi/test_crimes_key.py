import unittest
import requests
import json

class TestMovies(unittest.TestCase):

    SITE_URL = 'http://student13.cse.nd.edu:51069' # replace with your port number and
    print("testing for server: " + SITE_URL)
    CRIMES_URL = SITE_URL + '/crimes/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        c = {}
        r = requests.put(self.RESET_URL, data = json.dumps(c))


    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_crimes_get_key(self):
        self.reset_data()

        # fix with actual incident_number
        incident = 'I192062988'
        r = requests.get(self.CRIMES_URL + str(incident))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        #print(resp)
        self.assertEqual(resp['incident_number'], 'I192062988')
        self.assertEqual(resp['type'], 'INVESTIGATE PERSON')

    def test_crimes_put_key(self):
        self.reset_data()
        incident = 'I192062988'

        r = requests.get(self.CRIMES_URL + str(incident))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        print("Checking the get in put test")
        print(resp)
        self.assertEqual(resp['incident_number'], 'I192062988')
        self.assertEqual(resp['type'], 'INVESTIGATE PERSON')

        c = {}
        c['incident'] = 'ABC123'
        c['type'] = 'ASSAULT'
        r = requests.put(self.CRIMES_URL + str(incident), data = json.dumps(c))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        print("put response")
        print(resp)
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.CRIMES_URL + str(incident))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        #self.assertEqual(resp['incident_number'], c['incident'])
        self.assertEqual(resp['type'], c['type'])

    def test_crimes_delete_key(self):
        self.reset_data()
        incident = 'TESTTEST2'

        c = {}
        r = requests.delete(self.CRIMES_URL + str(incident), data = json.dumps(c))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.CRIMES_URL + str(incident))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

if __name__ == "__main__":
    unittest.main()
