import unittest
import requests
import json

class TestReset(unittest.TestCase):

    SITE_URL = 'http://student13.cse.nd.edu:51069' # replace with your port id
    print("Testing for server: " + SITE_URL)
    RESET_URL = SITE_URL + '/reset/'
    CRIMES_URL = SITE_URL + '/crimes/'

    def test_put_reset_index(self):
        c = {}
        r = requests.put(self.RESET_URL)

        incident = 'I192062988'

        # put a new crime in
        c['incident_number'] = 'NewTest'
        c['type'] = 'ASSAULT'
        r = requests.put(self.CRIMES_URL + str(incident), data = json.dumps(c))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        # call our reset index request
        r = requests.put(self.RESET_URL)
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        # do a get request and see if the movie was properly reset
        r = requests.get(self.CRIMES_URL + str(incident))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['incident_number'], 'I192062988')
        self.assertEqual(resp['type'], 'INVESTIGATE PERSON')

    def test_put_reset_key(self):
       incident = 'I192062988'

       c = {}
       c['incident_number'] = 'ABC123'
       c['type'] = 'Battery'
       r = requests.put(self.CRIMES_URL + str(incident), data = json.dumps(c))
       resp = json.loads(r.content.decode('utf-8'))
       print("put response")
       print(resp)
       self.assertEqual(resp['result'], 'success')

       # reset the data
       r = requests.put(self.RESET_URL + str(incident))
       resp = json.loads(r.content.decode('utf-8'))
       print("put reset response")
       print(resp)
       self.assertEqual(resp['result'], 'success')

       # check that the database was reset
       print("checking incident")
       print(incident)
       r = requests.get(self.CRIMES_URL + str(incident))
       resp = json.loads(r.content.decode('utf-8'))
       self.assertEqual(resp['result'], 'success')

       print("final get response")
       print(resp)

       self.assertEqual(resp['incident_number'], 'I192062988')
       self.assertEqual(resp['type'], 'INVESTIGATE PERSON')



if __name__ == "__main__":
    unittest.main()
