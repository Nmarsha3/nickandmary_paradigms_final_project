import cherrypy
import re, json
from ooapi.crime_library import _crime_database

class CrimeController(object):

        def __init__(self, mdb=None):
                if mdb is None:
                        self.mdb = _crime_database() # not ideal
                else:
                        self.mdb = mdb # correct behavior

                self.mdb.load_crimes('data/bostonCrime.csv')

        def GET_KEY(self, incident):
                '''when GET request for /crimes/incident_number comes in, then we respond with json string'''
                output = {'result':'success'} # this outputs if operation works

                try:
                        # making sure incident is a string
                        incident = str(incident)
                        # print(incident)
                        # getting the crime information
                        crime = self.mdb.get_crime_type(incident)
                        is_shooting = self.mdb.get_shooting(incident)
                        street = self.mdb.get_street(incident)
                        date = self.mdb.get_occurred_on_date(incident)
                        lat, lng = self.mdb.get_coordinates(incident)
                        #print("In GET_KEY")
                        #print(crime)

                        # if the crime exists in the data base, save the incident number and the type of crime
                        if crime is not None:
                                output['incident_number'] = incident
                                output['type'] = crime
                                output['shooting'] = is_shooting
                                output['street'] = street
                                output['date'] = date
                                output['latitude'] = lat
                                output['longitude'] = lng
                        else:
                                output['result'] = 'error'
                                output['message'] = 'crime type not found'
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def PUT_KEY(self, incident):
                '''when PUT request for /crime/incident comes in, then we change that crime i n the mdb'''
                output = {'result':'success'}

                # get data
                data = json.loads(cherrypy.request.body.read().decode('utf-8'))
                # print("In PUT_KEY")
                # print(data)

                # add information to list we will use to make a crime
                crime = list()
                crime.append("")
                crime.append("")
                crime.append(data["type"])
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")
                crime.append("")

                # Change crime's type
                self.mdb.set_crime(incident, crime)

                return json.dumps(output)

        def DELETE_KEY(self, incident):
                '''when DELETE for /crime/incident comes in, we remove just that crime from db'''
                output = {'result':'success'}

                try:
                   # remove crime from data base
                   self.mdb.delete_crime(incident)
                except Exception as ex:
                   output['result'] = 'error'
                   output['message'] = str(ex)

                return json.dumps(output)

        def GET_INDEX(self):
                '''when GET request for /crimes/ comes in, we respond with all the crime information in a json str'''
                output = {'result':'success'}
                output['crimes'] = []

                try:
                        # go thru the crimes
                        for incident in self.mdb.get_crimes():
                                # get the type for the specific crime
                                crime = self.mdb.get_crime_type(incident)
                                is_shooting = self.mdb.get_shooting(incident)
                                street = self.mdb.get_street(incident)
                                date = self.mdb.get_occurred_on_date(incident)
                                lat, lng = self.mdb.get_coordinates(incident)
                                # set specific info
                                dcrime = {'incident_number':incident, 'type':crime, 'shooting': is_shooting, 'street': street, 'date':date, 'latitude':lat, 'longitude':lng}
                                # add to output list
                                output['crimes'].append(dcrime)
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def POST_INDEX(self):
                '''when POST for /crimes/ comes in, we take title and genres from body of request, and respond with the new movie_id and more'''

                output = {'result' : 'success'}

                # load data
                data_json = json.loads(cherrypy.request.body.read().decode('utf-8'))
                print(data_json)

                try:
                   # get incident num
                   incident = data_json['incident_number']
                   # print(incident)
                   # get type
                   crime_type = data_json['type']
                   shooting = data_json['shooting']
                   occurred_on_date = data_json['occurred_on_date']
                   latitude = data_json['latitude']
                   longitude = data_json['longitude']
                   street = data_json['street']

                   # add info to output
                   output['incident_number'] = incident
                   output['type'] = crime_type

                   # Future idea: make crime a class
                   
                   # add info to crime list
                   crime = []
                   crime.append("")
                   crime.append("")
                   crime.append(crime_type)
                   crime.append("")
                   crime.append("")
                   crime.append(shooting)
                   crime.append(occurred_on_date)
                   crime.append("")
                   crime.append("")
                   crime.append("")
                   crime.append("")
                   crime.append("")
                   crime.append(street)
                   crime.append(latitude)
                   crime.append(longitude)
                   crime.append("")
                   
                   # set the crime
                   self.mdb.set_crime(incident, crime)
                except Exception as ex:
                   output['result'] = 'error'
                   output['message'] = str(ex)
                
                #print(output)
                return json.dumps(output)

        def DELETE_INDEX(self):
                '''when DELETE for /crime/ comes in, we remove each existing crime from mdb object'''
                output = {'result':'success'}

                try:
                   # go thru crimes by incident num
                   for incident in list(self.mdb.get_crimes()):
                      # delete crime
                      self.mdb.delete_crime(incident)
                except Exception as ex:
                   output['result'] = 'error'
                   output['message'] = str(ex)

                return json.dumps(output)

