import unittest
import requests
import json

class TestCrimesIndex(unittest.TestCase):

    SITE_URL = 'http://student13.cse.nd.edu:51069'
    print("Testing for server: " + SITE_URL)
    CRIMES_URL = SITE_URL + '/crimes/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
       m = {}
       r = requests.put(self.RESET_URL, json.dumps(m))
    
    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_crimes_index_post(self):
        self.reset_data()

        c = {}
        c['incident_number'] = 'ABC123'
        c['type'] = 'Assault'
        #crimes_u = self.CRIMES_URL + '/ABC123/'
        #r = requests.post(self.CRIMES_URL, data = json.dumps(c))
        r = requests.post('http://student06.cse.nd.edu:51069/crimes/', data = json.dumps(c))
        print(r.content.decode())
        self.assertTrue(self.is_json(r.content.decode("utf-8")))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['incident_number'], 'ABC123')

        r = requests.get(self.CRIMES_URL + str(resp['incident_number']))
        print(r.content.decode())
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['incident_number'], c['incident_number'])
        self.assertEqual(resp['type'], c['type'])

    def test_crimes_index_delete(self):
        self.reset_data()

        c = {}
        r = requests.delete(self.CRIMES_URL, data = json.dumps(c))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.CRIMES_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        crimes = resp['crimes']
        self.assertFalse(crimes)

if __name__ == "__main__":
    unittest.main()
