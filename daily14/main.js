console.log('page load - entered main.js for js-other api');

var sendButton = document.getElementById('send-button');
sendButton.onmouseup = getFormInfo;
var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = clearForm;

// Resets the response text to original state
function clearForm(){

   console.log("entered clearForm");
   
   var label = document.getElementById('response-label');
   var label2 = document.getElementById('answer-label');

   label.innerHTML = 'Response:';
   label2.innerHTML = '-';

}

function getFormInfo(){
    console.log('entered getFormInfo!');

    // get dropdown select-server-address
    var selindex = document.getElementById("select-server-address").selectedIndex;
    var url_base = document.getElementById('select-server-address').options[selindex].value;
    
    // get input from simple textbox
    var port_num = document.getElementById('input-port-number').value;

    // get values from radio buttons
    var action = "GET";
    if(document.getElementById('radio-get').checked){
        action = 'GET';
    }
    else if(document.getElementById('radio-put').checked){
        action = 'PUT';
    }
    else if(document.getElementById('radio-post').checked){
        action = 'POST';
    }
    else if(document.getElementById('radio-delete').checked){
        action = 'DELETE';
    }

    // get status from a checkbox
    var key = null;
    if(document.getElementById('checkbox-use-key').checked){
        key = document.getElementById('input-key').value;
    }

    // get status from  a message box
    var message_body = null;
    if(document.getElementById('checkbox-use-message').checked){
        // get key value
        message_body = document.getElementById('text-message-body').value;
    }

    console.log("url_base = ", url_base);
    console.log("port_num = ", port_num);
    console.log("action = ", action);
    console.log("key = ", key);
    console.log("message_body = ", message_body);
    makeNetworkCall(url_base, port_num, action, key, message_body);

}

// Used grabbed data to make network call
function makeNetworkCall(url_base, port_num, action, key, message_body){
    
   console.log('entered makeNetworkCall');

   var xhr = new XMLHttpRequest();

   // Make url based on if we have a key
   if(key == null){
      var url = url_base + ':' + port_num + '/movies/';
   }
   else{
      var url = url_base + ':' + port_num + '/movies/' + key;
   }

   // Make call
   xhr.open(action, url, true);
   xhr.onload = function(e){
      console.log('Response is ' + xhr.responseText);
      updateIndexWithResponse(xhr.responseText, action, key);
   }
   xhr.onerror = function(e){
      console.log('Error is ' + xhr.statusText);
   }

   // Send body if we have one
   if(message_body == null){
      xhr.send(null);
   }
   else{
      console.log('message = ' + message_body);
      xhr.send(message_body);
   }

}

// Updates the Response box
function updateIndexWithResponse(response_text, action, key){

   console.log("in updateIndexWithResponse");

   // grab labels
   var label = document.getElementById('response-label');
   var label2 = document.getElementById('answer-label');

   // Update raw data label
   label.innerHTML = response_text;

   // Load the data as JSON
   response_json = JSON.parse(response_text);

   console.log('response json = '+ response_json);

   // Parse data and display
   if(action == 'GET'){
      console.log("key = " + key);
      console.log("result = " + response_json['result']);
      if (key != null && response_json['result'] == 'success'){
         console.log("In if2")
         label2.innerHTML = response_json['title'] + ' belongs to genres: ' + response_json['genres'];
      }
      else{
         label2.innerHTML = '-';
      }
   }
   else{
      label2.innerHTML = '-'
   }

}


// Function used for testing network calls
function make_test_network_call(){
   console.log('Entered the test function');
   
   var xhr = new XMLHttpRequest();
   xhr.open('GET', 'http://student13.cse.nd.edu:51069/movies/5', true);
   xhr.onload = function(e){
      console.log('Response is ' + xhr.responseText);
   }
   xhr.onerror = function(e){
      console.log('Error is ' + xhr.statusText);
   }
   xhr.send(null); // null is where the body goes

}


