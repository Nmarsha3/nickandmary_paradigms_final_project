import cherrypy
import sys
import os

sys.path.append(".")

from ooapi.resetController import ResetController
from ooapi.crimeController import CrimeController
from ooapi.crime_library import _crime_database

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    mdb = _crime_database() # from crime_library.py

    crimeController     = CrimeController(mdb=mdb)
    resetController     = ResetController(mdb=mdb)

    dispatcher.connect('crime_get', '/crimes/:incident', controller=crimeController, action = 'GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('crime_put', '/crimes/:incident', controller=crimeController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('crime_delete', '/crimes/:incident', controller=crimeController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('crime_index_get', '/crimes/', controller=crimeController, action = 'GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('crime_index_post', '/crimes/', controller=crimeController, action = 'POST_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('crime_index_delete', '/crimes/', controller=crimeController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))

    dispatcher.connect('reset_put', '/reset/:incident', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

    # CORS related options connections
    dispatcher.connect('crime_key_options', '/crimes/:incident', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('crime_options', '/crimes/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_key_options', '/reset/:incident', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/reset/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    conf = {
        'global': {
            'server.thread_pool': 5, # optional argument
            'server.socket_host': 'student13.cse.nd.edu', #
            'server.socket_port': 51069, #change port number to your assigned
            },
        '/': {
            'request.dispatch': dispatcher,
            'tools.CORS.on':True,
            }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

# end of start_service


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) # turn cors tools on
    start_service()

