console.log('page load - entered crimeMain.js');

var sendButton = document.getElementById('send-button');
sendButton.onmouseup = getFormInfo;
var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = clearForm;

// Resets the response text to original state
function clearForm(){

   console.log("entered clearForm");
   
   var label = document.getElementById('response-label');
   var label2 = document.getElementById('answer-label');

   label.innerHTML = 'Response:';
   label2.innerHTML = '-';

}

function getFormInfo(){
    console.log('entered getFormInfo!');

    // get dropdown select-crime-type
     var selindex = document.getElementById("select-crime-type").selectedIndex;
     var crime_type = document.getElementById('select-crime-type').options[selindex].value;

    console.log("crime_type = ", crime_type);
    makeNetworkCall(crime_type);

}

// make network call
function makeNetworkCall(crime_type){
    
   console.log('entered makeNetworkCall');

   var xhr = new XMLHttpRequest();

   // Make url
   var url = 'http://student13.cse.nd.edu:51069/crimes/';
   console.log('url: ' + url);

   // Make call
   xhr.open("GET", url, true);
   xhr.onload = function(e){
      console.log('Response is ' + xhr.responseText);
      updateIndexWithResponse(xhr.responseText, crime_type.toUpperCase());
   }
   xhr.onerror = function(e){
      console.log('Error is ' + xhr.statusText);
   }
   xhr.send(null);

}

// Updates the Response box
function updateIndexWithResponse(response_text, crime_type, shootings_only){

   console.log("in updateIndexWithResponse");

   // grab labels
   var label = document.getElementById('response-label');
   var label2 = document.getElementById('answer-label');

   response_json = JSON.parse(response_text);

   // Loop through json response data
   var crimes = response_json['crimes'];
   //var lats = response_json['latitude'];
   //var longs = response_json['longitute'];
   console.log("printing crimes");
   console.log(crimes);
   print_string = "";
   count = 0;
   for(var i = 0; i < crimes.length; i++){
      if(crimes[i]["type"].includes(crime_type)){
         count += 1
         print_string += crimes[i]["incident_number"] + ": " + crimes[i]["type"] + " on " + crimes[i]["street"] + ", date: " + crimes[i]["date"] + "<BR/>";
         if(crimes[i]['latitude'] == "" || crimes[i]['latitude'] == ""){
            continue;
         }
         console.log(crimes[i]['latitude']);
      }
   }

   console.log("printing print_string");
   console.log(print_string);

   // Update raw data label
   label.innerHTML = "Number of " + crime_type + "'s: " + count;
   label2.innerHTML = print_string;

}





