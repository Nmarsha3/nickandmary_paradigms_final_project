console.log('page load - entered crimeMain.js');

var sendButton = document.getElementById('search-button');
sendButton.onmouseup = getFormInfo;
var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = clearForm;


// Resets the response text to original state
function clearForm(){

   console.log("entered clearForm");
   
   var label = document.getElementById('response-label');
   var label2 = document.getElementById('answer-label');

   label.innerHTML = 'Response:';
   label2.innerHTML = '-';

}

function getFormInfo(){
    console.log('entered getFormInfo!');

    // get incident number
    incident = document.getElementById('incident').value;

    console.log("incident = ", incident);

    makeNetworkCall(incident);

}

// Used grabbed data to make network call
function makeNetworkCall(incident){
    
   console.log('entered makeNetworkCall');

   var xhr = new XMLHttpRequest();

   // Make url
   var url = 'http://student13.cse.nd.edu:51069/crimes/' + incident;
   console.log('url: ' + url);

   // Make call
   xhr.open("GET", url, true);
   xhr.onload = function(e){
      console.log('Response is ' + xhr.responseText);
      updateIndexWithResponse(xhr.responseText, incident);
   }
   xhr.onerror = function(e){
      console.log('Error is ' + xhr.statusText);
   }
   xhr.send(null);

}

// Updates the Response box
function updateIndexWithResponse(response_text, incident){

   console.log("in updateIndexWithResponse");

   // grab labels
   var label = document.getElementById('response-label');
   var label2 = document.getElementById('answer-label');

   // Update raw data label
   //label.innerHTML = response_text;

   // Load the data as JSON
   response_json = JSON.parse(response_text);

   console.log('response json = '+ response_json);

   // Parse data for lat and long
   lat = response_json['latitude'];
   lng = response_json['longitude'];
   initMap(parseFloat(lat), parseFloat(lng));


   // Parse data and display
   console.log("result = " + response_json['result']);
   if (response_json['result'] == 'success'){
      label2.innerHTML = "Incident number: " + response_json['incident_number'] + "<br>" + "Type: " + response_json['type'] + "<br>" + "Shooting: " + response_json['shooting'] + "<br>" + "Latitude: " + lat + "<br>" + "Longitude: " + lng +  "<br>" + "Street: " + response_json['street'] + "<br>" + "Date: " + response_json['date'];
   }
   else{
      label2.innerHTML = 'Server problem: Result was not success';
   }

}

function initMap(new_lat=0, new_lng=0) { 
   var test= {lat: new_lat, lng: new_lng}; 
   var map = new google.maps.Map(document.getElementById('map'), { 
     zoom: 11, 
     center: test 
   }); 
   var marker = new google.maps.Marker({ 
     position: test, 
     map: map 
   }); 
 } 
