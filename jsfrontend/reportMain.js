console.log('page load - entered reportMain.js');

var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = getFormInfo;
var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = clearForm;


// Resets the response text to original state
function clearForm(){

   console.log("entered clearForm");
   
   var label = document.getElementById('response-label');
   var label2 = document.getElementById('answer-label');

   label.innerHTML = 'Response:';
   label2.innerHTML = '-';

}

function getFormInfo(){
    console.log('entered getFormInfo!');

    // get incident number
    var incident = document.getElementById('incident').value;
    console.log("incident = ", incident);
    incident = String(incident);
    
    // get crime type
    var type = document.getElementById('type').value;
    console.log("type = ", type);
    type = String(type);

    // get date
    var date = document.getElementById('date').value;
    console.log("date = ", date);
    date = String(date);

    // get street
    var street = document.getElementById('street').value;
    console.log("street = ", street);
    street = String(street);

    // get lat
    var lat = document.getElementById('latitude').value;
    console.log("lat = ", lat);
    lat = String(lat);

    // get longitude
    var lng = document.getElementById('longitude').value;
    console.log("lng = ", lng);
    lng = String(lng);

    // get values from radio buttons
    var ans = "0";
    if(document.getElementById('radio-yes').checked){
        ans = "1";
    }
    else if(document.getElementById('radio-no').checked){
        ans = "0";
    }

    makeNetworkCall(incident, type, date, street, lat, lng, ans);
}

// Used grabbed data to make network call
function makeNetworkCall(incident, type, date, street, lat, lng, shooting){
   console.log('entered makeNetworkCall');

   var xhr = new XMLHttpRequest();

   // Make url
   var url = 'http://student13.cse.nd.edu:51069/crimes/';
   console.log('url: ' + url);

   // make message body
   var message_body = "{\"incident_number\":\"" + incident + "\",\"type\":\"" + type + "\",\"shooting\": \"" + shooting + "\",\"latitude\":\"" + lat + "\",\"longitude\":\"" + lng + "\",\"street\":\"" + street + "\",\"occurred_on_date\":\"" + date + "\"}";

   console.log('message body: ' + message_body);

   // Make call
   xhr.open("POST", url, true);
   xhr.onload = function(e){
      console.log('Response is ' + xhr.responseText);
      updateIndexWithResponse(xhr.responseText, incident, type, date, street, lat, lng, shooting);
   }
   xhr.onerror = function(e){
      console.log('Error is ' + xhr.statusText);
   }
   xhr.send(message_body);

}

// Updates the Response box
function updateIndexWithResponse(response_text, incident, type, date, street){

   console.log("in updateIndexWithResponse");

   // grab labels
   var label = document.getElementById('answer-label');

   // Load the data as JSON
   response_json = JSON.parse(response_text);

   console.log('response json = '+ response_json);

   // Parse data for lat and long
   var res = response_json['result'];

   // Parse data and display
   console.log("result = " + response_json['result']);
   if (response_json['result'] == 'success'){
      label.innerHTML = "You successfully reported a crime. You reported a/an " + type + " that occurred on " + street + " on " + date;
   }
   else{
      label.innerHTML = 'Server problem: Result was not success';
   }

}