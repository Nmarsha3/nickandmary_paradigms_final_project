console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    var name = document.getElementById("name-text").value;
    console.log('Number you entered is ' + name);
    makeNetworkCallToAgeApi(name);
}

function makeNetworkCallToAgeApi(name){
    console.log('entered make nw call ' + name);
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.adviceslip.com/advice/" + name;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr
    xhr.onload = function(e) { 
        // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        console.log("im here")
        updateAgeWithResponse(name, xhr.responseText);
    }
    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }
    // actually make the network call
    xhr.send(null)
}

function updateAgeWithResponse(name, response_text){
    var st = "}"
    var resp = response_text.concat(st);
    var response_json = JSON.parse(resp);
    var label1 = document.getElementById("response-line1");

    //console.log(response_json['advice'])

    if(response_json['slip']['id'] == null){
        label1.innerHTML = 'Apologies, we could not find advice for you.'
    } else{
        label1.innerHTML =  'Your advice is: ' + response_json['slip']['advice'];
        //var age = parseInt(response_json['slip']['advice']);
        makeNetworkCallToNumbers(name);
    }
} 

function makeNetworkCallToNumbers(age){
    console.log('entered make nw call' + age);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://swapi.dev/api/people/" + age + "/";
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateTriviaWithResponse(age, xhr.responseText);
    }
    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }
    xhr.send(null) // last step - this actually makes the request
} 

function updateTriviaWithResponse(age, response_text){
    // update a label
    var label2 = document.getElementById("response-line2");
    var response_json = JSON.parse(response_text);
    label2.innerHTML = "Your Star Wars character is " + response_json['name'] + " and his/her hair color is " + response_json['hair_color'] + ".";

    // dynamically adding label
    label_item = document.createElement("label"); // "label" is a classname
    label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object

    var item_text = document.createTextNode(response_json); // creating new text
    label_item.appendChild(item_text); // adding something to button with appendChild()
} 